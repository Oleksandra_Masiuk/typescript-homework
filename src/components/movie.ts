import { NOT_LIKED_RED, RED } from '../constants/colors';
import { FAVORITE } from '../constants/id';
import { MovieType } from '../enums/movieType';
import { getRandomNumber } from '../helpers/random';
import { IMovie } from '../interfaces/interfaces';
import ApiService from '../services/apiService';
import { createFilmCard, createRandomFilmCard } from '../services/domService';
import { dislikeFilm, getLikedFilms, likeFilm } from '../services/storage';

const apiService = new ApiService();

let randomFilmId: number | null = null;

async function showRandomFilm(id: number): Promise<void> {
  const movie = await apiService.getFilmById(id);
  const randomMovie = document.getElementById('random-movie');

  if (!randomMovie)
    return console.error('not found html element with id random-movie');

  if (randomMovie) {
    if (!movie) return console.error(`not found movie with id ${id}`);

    if (movie?.backdropPath) {
      randomMovie.setAttribute(
        'style',
        `background: center / cover no-repeat url("${movie.backdropPath}");`
      );
    }

    const randomElement = createRandomFilmCard(movie);
    randomMovie.appendChild(randomElement);
  }
}

async function showFirstFilms(): Promise<void> {
  const movies = await apiService.getFilms(MovieType.POPULAR);

  if (!movies) return console.error(`not found movies`);

  const random = getRandomNumber(0, 19);
  randomFilmId = movies[random]?.id;

  if (randomFilmId) showRandomFilm(randomFilmId);

  await showFilms(movies);
  history.pushState(null, '', '/');
}

async function showFilms(movies: IMovie[]): Promise<void> {
  if (movies.length === 0) {
    if (apiService.page !== 1) return;

    const container = document.getElementById('film-container');

    if (!container)
      return console.error('not found html element with id film-container');

    container.innerHTML = '';
    return;
  }
  if (apiService.page !== 1) {
    const urlParams = new URLSearchParams(window.location.search);
    urlParams.set('page', apiService.page.toString());
    history.pushState(null, '', '?' + urlParams.toString());
  }

  const filmContainer = document.getElementById('film-container');
  removeEventListenerLiked();

  if (filmContainer && apiService.page === 1) filmContainer.innerHTML = '';
  const likedFilms: number[] = getLikedFilms();
  movies.forEach((movie) => {
    let isLiked = false;

    if (likedFilms.includes(movie.id)) isLiked = true;
    const element = createFilmCard(movie, isLiked, false);
    filmContainer?.appendChild(element);
  });
  addEventListenerLiked();
}

async function showLoadedFilms(type: string): Promise<void> {
  if (apiService.category !== type) {
    apiService.resetPage();
    history.pushState(null, '', '/');
    apiService.changeCategory(<MovieType>type);
  }

  const movies = await apiService.getFilms(type);
  await showFilms(movies);
}

async function showSearchedFilms(query: string): Promise<void> {
  if (apiService.category !== MovieType.SEARCH) {
    apiService.resetPage();
    apiService.changeCategory(MovieType.SEARCH);
    history.pushState(null, '', '/');
  }

  if (apiService.query !== query) {
    apiService.resetPage();
    apiService.changeQuery(query);
    history.pushState(null, '', '/');
  }

  const movies = await apiService.searchFilm();
  await showFilms(movies);
}

function addEventListenerLiked(): void {
  document.querySelectorAll('svg.bi-heart-fill').forEach((element) => {
    element.addEventListener('click', toggleLike);
  });
}

function removeEventListenerLiked(): void {
  document.querySelectorAll('svg.bi-heart-fill').forEach((element) => {
    element.removeEventListener('click', toggleLike);
  });
}

function toggleLike(this: HTMLElement) {
  const id = Number(this.dataset.value);
  const likedFilms: number[] = getLikedFilms();
  const isLiked = likedFilms.includes(id);
  const heartElements = document.querySelectorAll<HTMLElement>(
    `[data-value="${id}"]`
  );

  if (isLiked) {
    dislikeFilm(id);

    heartElements.forEach((element) =>
      element.setAttribute('fill', NOT_LIKED_RED)
    );

    heartElements.forEach((element) => {
      if (element.id.includes(FAVORITE)) {
        const filmContainer = document.getElementById('favorite-movies');
        const parent = <HTMLElement>element.parentNode?.parentNode;
        filmContainer?.removeChild(parent);
      }
    });
  } else {
    likeFilm(id);
    heartElements.forEach((element) => element.setAttribute('fill', RED));
  }
}

async function loadMore(): Promise<void> {
  apiService.incrementPage();

  if (apiService.category !== MovieType.SEARCH) {
    await showLoadedFilms(apiService.category);
    return;
  }

  await showSearchedFilms(apiService.query);
}

function showLikedFilms(): void {
  const filmsContainer = document.getElementById('favorite-movies');

  if (!filmsContainer)
    return console.error('not found html element favorite-movies');

  filmsContainer.innerHTML = '';

  const favoriteFilms = getLikedFilms();
  favoriteFilms.forEach((id) => showLikedFilm(id));
}

async function showLikedFilm(id: number): Promise<void> {
  const filmContainer = document.getElementById('favorite-movies');

  if (!filmContainer)
    return console.error('not found html element favorite-movies');

  const movie = await apiService.getFilmById(id);
  filmContainer.appendChild(createFilmCard(<IMovie>movie, true, true));

  removeEventListenerLiked();
  addEventListenerLiked();
}

export {
  showLoadedFilms,
  showSearchedFilms,
  showRandomFilm,
  showLikedFilms,
  addEventListenerLiked,
  removeEventListenerLiked,
  loadMore,
  showFirstFilms,
};

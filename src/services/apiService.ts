import { API_KEY, BASE_URL } from '../constants/api';
import axios from 'axios';
import { IMovie } from '../interfaces/interfaces';
import { mapper, itemMapper } from '../helpers/mapper';
import { MovieType } from '../enums/movieType';

export default class ApiService {
  page = 1;
  category: MovieType = MovieType.POPULAR;
  query = '';

  async searchFilm(): Promise<IMovie[]> {
    const url = `${BASE_URL}/search/movie?api_key=${API_KEY}&language=en-US&query=${this.query}}&page=${this.page}&include_adult=false`;
    const {
      data: { results },
    } = await axios.get(url);

    let newMovies: IMovie[] = [];
    if (results) newMovies = mapper(results);

    return newMovies;
  }

  async getFilms(type: string): Promise<IMovie[]> {
    const url = `${BASE_URL}/movie/${type}?api_key=${API_KEY}&language=en-US&page=${this.page}`;
    const {
      data: { results },
    } = await axios.get(`${url}`);

    let newMovies: IMovie[] = [];
    if (results) newMovies = mapper(results);

    return newMovies;
  }

  async getFilmById(id: number): Promise<IMovie | null> {
    const url = `${BASE_URL}/movie/${id}?api_key=${API_KEY}&language=en-US`;
    const { data } = await axios.get(url);

    if (data) return itemMapper(data);
    
    return null;
  }

  resetPage(): void {
    this.page = 1;
  }

  incrementPage(): void {
    this.page += 1;
  }

  changeCategory(category: MovieType): void {
    this.category = category;
  }

  changeQuery(query: string): void {
    this.query = query;
  }
}

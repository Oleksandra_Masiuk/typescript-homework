import { RED, NOT_LIKED_RED } from '../constants/colors';
import { FAVORITE, USUAL } from '../constants/id';
import { IAttribute, IMovie } from '../interfaces/interfaces';

function createFilmCard(
  movie: IMovie,
  isLiked: boolean,
  isFav: boolean
): HTMLElement {
  const elClass = isFav ? 'col-12 p-2' : 'col-lg-3 col-md-4 col-12 p-2';
  const Element = createElement('div', elClass);
  const Card = createElement('div', 'card shadow-sm');
  const Image = createElement('img', '', {
    src: `${movie.posterPath}`,
    alt: 'poster',
  });
  const cardBody = createElement('div', 'card-body');
  const cardText = createElement('p', 'card-text truncate');
  cardText.innerText = movie.overview;

  const dateWrapper = createElement(
    'div',
    'd-flex justify-content-between align-items-center'
  );

  const date = createElement('small', 'text-muted');
  date.textContent = movie.releaseDate;

  dateWrapper.appendChild(date);
  cardBody.append(cardText, dateWrapper);
  Card.append(Image);

  const color = isLiked ? RED : NOT_LIKED_RED;
  const name = isFav ? `${FAVORITE}-${movie.id}` : `${USUAL}-${movie.id}`;
  Card.innerHTML += `
        <svg
        id="${name}"
            data-value="${movie.id}"
            xmlns="http://www.w3.org/2000/svg"
            stroke="red"
            fill="${color}"
            width="50"
            height="50"
            class="bi bi-heart-fill position-absolute p-2"
            viewBox="0 -2 18 22"
        >
        <path
            fill-rule="evenodd"
            d="M8 1.314C12.438-3.248 23.534 4.735 8 15-7.534 4.736 3.562-3.248 8 1.314z"
        />
        </svg>`;

  Card.append(cardBody);
  Element.appendChild(Card);

  return Element;
}

function createRandomFilmCard(film: IMovie): HTMLElement {
  const element = createElement('div', 'row py-lg-5');
  const background = createElement('div', 'col-lg-6 col-md-8 mx-auto');

  const name = createElement('h1', 'fw-light text-light', {
    id: 'random-movie-name',
  });
  name.textContent = film.title;

  const description = createElement('p', 'lead text-white', {
    id: 'random-movie-description',
  });
  description.textContent = film.overview;

  background.appendChild(name);
  background.appendChild(description);
  element.appendChild(background);

  return element;
}

function createElement(
  tagName: string,
  className: string,
  attributes: IAttribute = {}
): HTMLElement {
  const element = document.createElement(tagName);

  if (className) {
    const classNames = className.split(' ').filter(Boolean);
    element.classList.add(...classNames);
  }

  Object.keys(attributes).forEach((key) =>
    element.setAttribute(key, attributes[key])
  );

  return element;
}

export { createFilmCard, createRandomFilmCard };

import { LOCAL_STORAGE_ARRAY } from '../constants/storage';

export function likeFilm(id: number): void {
  const items = localStorage.getItem(LOCAL_STORAGE_ARRAY);
  const likedFilms: number[] = items ? JSON.parse(items) : [];
  if (!likedFilms.includes(id)) {
    likedFilms.push(id);
  }
  localStorage.setItem(LOCAL_STORAGE_ARRAY, JSON.stringify(likedFilms));
}

export function dislikeFilm(id: number): void {
  const items = localStorage.getItem(LOCAL_STORAGE_ARRAY);
  if (!items) {
    return console.error(
      `not found element ${LOCAL_STORAGE_ARRAY} at local storage`
    );
  }
  const likedFilms: number[] = JSON.parse(items);
  const index = likedFilms.indexOf(id);
  if (index !== -1) {
    likedFilms.splice(index, 1);
  }
  localStorage.setItem(LOCAL_STORAGE_ARRAY, JSON.stringify(likedFilms));
}

export function getLikedFilms(): number[] {
  const items = localStorage.getItem(LOCAL_STORAGE_ARRAY);
  if (items) {
    return <number[]>JSON.parse(items);
  } else {
    return [];
  }
}

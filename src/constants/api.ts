const API_KEY = 'd5fb52a706c708d7b8e3adfd2b759d68';
const BASE_URL = 'https://api.themoviedb.org/3';
const BASE_IMAGE_URL = 'https://image.tmdb.org/t/p/w500';
const IMG_PLACEHOLDER = 'https://critics.io/img/movies/poster-placeholder.png';

export { API_KEY, BASE_URL, BASE_IMAGE_URL, IMG_PLACEHOLDER };

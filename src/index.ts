import {
  showLoadedFilms,
  showSearchedFilms,
  showRandomFilm,
  showLikedFilms,
  addEventListenerLiked,
  removeEventListenerLiked,
  loadMore,
  showFirstFilms,
} from './components/movie';
import { MovieType } from './enums/movieType';
export async function render(): Promise<void> {
  showFirstFilms();
  showRandomFilm;
  removeEventListenerLiked();
  addEventListenerLiked();
  document
    .getElementById(MovieType.TOP_RATED)
    ?.addEventListener('click', () => {
      showLoadedFilms(MovieType.TOP_RATED);
    });

  document.getElementById(MovieType.UPCOMING)?.addEventListener('click', () => {
    showLoadedFilms(MovieType.UPCOMING);
  });

  document.getElementById(MovieType.POPULAR)?.addEventListener('click', () => {
    showLoadedFilms(MovieType.POPULAR);
  });

  document.getElementById('submit')?.addEventListener('click', () => {
    const { value } = <HTMLInputElement>document.getElementById('search');
    showSearchedFilms(value);
  });

  document
    .getElementsByClassName('navbar-toggler')[0]
    ?.addEventListener('click', () => {
      showLikedFilms();
      removeEventListenerLiked();
      addEventListenerLiked();
    });

  document
    .getElementsByClassName('btn-close')[0]
    ?.addEventListener('click', () => {
      removeEventListenerLiked();
      addEventListenerLiked();
    });
  document.getElementById('load-more')?.addEventListener('click', loadMore);
}

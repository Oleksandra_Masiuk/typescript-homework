enum MovieType {
  TOP_RATED = 'top_rated',
  POPULAR = 'popular',
  UPCOMING = 'upcoming',
  SEARCH = 'search',
}

export { MovieType };

import { IMovie } from '../interfaces/interfaces';
import { BASE_IMAGE_URL, IMG_PLACEHOLDER } from '../constants/api';

function mapper(films: Record<string, number | string | null>[]): IMovie[] {
  const mappedFilms = films.map((film) => <IMovie>itemMapper(film));
  return <IMovie[]>mappedFilms;
}

function itemMapper(item: Record<string, number | string | null>): IMovie {
  const { id, overview, release_date, title } = item;

  const poster = item.poster_path
    ? BASE_IMAGE_URL + item.poster_path
    : IMG_PLACEHOLDER;
  const backdrop = item.backdrop_path
    ? BASE_IMAGE_URL + item.backdrop_path
    : IMG_PLACEHOLDER;

  return <IMovie>{
    id,
    posterPath: poster,
    overview,
    backdropPath: backdrop,
    releaseDate: release_date,
    title,
  };
}

export { mapper, itemMapper };

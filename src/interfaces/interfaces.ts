interface IMovie {
  id: number;
  posterPath: string | null;
  overview: string;
  releaseDate: string;
  title: string;
  backdropPath: string | null;
}

interface IAttribute {
  [key: string]: string;
}

export { IMovie, IAttribute };
